import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

const serviceAccount = require('../configs/account-key.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://caffeapp-sandbox.firebaseio.com',
});

const etalon = {
  "employeesNumber": "20",
  "website": "any-italian-cafe.com",
  "tripadvisorRank": "5",
  "workingUpHours": 23,
  "city": "Racoon",
  "signatureDish": "Pizza",
  "phone": "(555) 555-1234",
  "address": "First Street, 1 ",
  "averageReceipt": "10",
  "chef": "Some Famous Chef",
  "name": "First cafe",
  "specialization": "Italian food",
  "workingFromHours": 14,
  "createdAt": '',
};
const count = 100;

const cache = {
  data: '',
  ttl: new Date(),
}

export const refill = functions.https.onRequest(async (request, response) => {
  try {
    const querySnapshot = await admin.firestore().collection("cafes").get();
    if (!querySnapshot.empty) {
      await querySnapshot.docs.map(async function (documentSnapshot) {
        await admin.firestore().collection("cafes").doc(documentSnapshot.id).delete();
        return documentSnapshot.id;
      })
    } else {
      console.log('no documents found');
    }
    for (const i of Array.from(Array(count).keys())) {
      const item = {
        "employeesNumber": etalon.employeesNumber + ' ' + i,
        "website": etalon.website + ' ' + i,
        "tripadvisorRank": etalon.tripadvisorRank + ' ' + i,
        "workingUpHours": etalon.workingUpHours + ' ' + i,
        "city": etalon.city + ' ' + i,
        "signatureDish": etalon.signatureDish + ' ' + i,
        "phone": etalon.phone + ' ' + i,
        "address": etalon.address + ' ' + i,
        "averageReceipt": etalon.averageReceipt + ' ' + i,
        "chef": etalon.chef + ' ' + i,
        "name": etalon.name + ' ' + i,
        "specialization": etalon.specialization + ' ' + i,
        "workingFromHours": etalon.workingFromHours + ' ' + i,
        "createdAt": (new Date()).toString(),
      }
      await admin.firestore().collection("cafes").add(item);
    }
    response.send('ADDED! ' + count);
  } catch (err) {
    response.send('ERROR!' + err);
  }
});

async function getList() {
  const querySnapshot = await admin.firestore().collection("cafes").orderBy('name').limit(100).get();
  let items: any[] = []
  if (!querySnapshot.empty) {
    items = querySnapshot.docs.map(function (documentSnapshot) {
      return documentSnapshot.data();
    })
  }
  return items;
}

export const list = functions.https.onRequest(async (request, response) => {
  if (cache.data.length > 0 && cache.ttl > new Date()) {
    console.log(`returned from cache`);
    response.status(200).send('returned from cache. ' + cache.data);
    return;
  }
  let data: string;
  try {

    let items = await getList();
    console.log(`loaded from firestore`);
    if (items.length) {
      data = 'items' + JSON.stringify(items.map(o => o.name));
    } else {
      data = 'no documents found';
    }
  } catch (err) {
    data = 'ERROR!';
  }
  cache.data = data + '';

  const dateInOneMinute = new Date()
  dateInOneMinute.setMinutes(dateInOneMinute.getMinutes() + 1);
  cache.ttl = dateInOneMinute

  response.status(200).send('loaded from firestore. ' + data);
});